﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPowerPlanSwitcher
{
    public class AppConfiguration
    {
        public string HighPerformanceGuid { get; set; }
        public string PowerSaverGuid { get; set; }
        public float HighPerformanceUsageThresholdAbove { get; set; }
        public float HighPerformanceSingleCoreUsageThresholdAbove { get; set; }
        public float HighPerformanceMinimumDuration { get; set; }
        public float HighPerformanceTransitionRequiredDuration { get; set; }
        public float PowerSaverUsageThresholdBelow { get; set; }
        public float PowerSaverSingleCoreUsageThresholdBelow { get; set; }
        public float PowerSaverTransitionRequiredDuration { get; set; }
        public float PollingRateTransitionSec { get; set; }
        public float PollingRatePowerSaver { get; set; }
        public float PollingRateHighPerformance { get; set; }
        public bool PlaySound { get; set; }

        public AppConfiguration()
        {
            HighPerformanceGuid = ConfigurationManager.AppSettings["HighPerformancePlanGuid"];
            PowerSaverGuid = ConfigurationManager.AppSettings["PowerSaverPlanGuid"];
           
            HighPerformanceUsageThresholdAbove = Convert.ToSingle(ConfigurationManager.AppSettings["HighPerformanceUsageThresholdAbove"]);
            HighPerformanceSingleCoreUsageThresholdAbove = Convert.ToSingle(ConfigurationManager.AppSettings["HighPerformanceSingleCoreUsageThresholdAbove"]);
            HighPerformanceMinimumDuration = Convert.ToSingle(ConfigurationManager.AppSettings["HighPerformanceMinimumDuration"]);
            HighPerformanceTransitionRequiredDuration = Convert.ToSingle(ConfigurationManager.AppSettings["HighPerformanceTransitionRequiredDuration"]);
           
            PowerSaverUsageThresholdBelow = Convert.ToSingle(ConfigurationManager.AppSettings["PowerSaverUsageThresholdBelow"]);
            PowerSaverSingleCoreUsageThresholdBelow = Convert.ToSingle(ConfigurationManager.AppSettings["PowerSaverSingleCoreUsageThresholdBelow"]);
            PowerSaverTransitionRequiredDuration = Convert.ToSingle(ConfigurationManager.AppSettings["PowerSaverTransitionRequiredDuration"]);

            PollingRateTransitionSec = Convert.ToSingle(ConfigurationManager.AppSettings["PollingRateTransitionSec"]);
            PollingRatePowerSaver = Convert.ToSingle(ConfigurationManager.AppSettings["PollingRatePowerSaverSec"]);
            PollingRateHighPerformance = Convert.ToSingle(ConfigurationManager.AppSettings["PollingRateHighPerformanceSec"]);
            PlaySound = Convert.ToBoolean(ConfigurationManager.AppSettings["PlaySound"]);
        }
    }
}
