﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AutoPowerPlanSwitcher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            // sleep to allow time to attach to process
            System.Threading.Thread.Sleep(5000);
#endif
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new AutoPowerPlanSwitcherService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
