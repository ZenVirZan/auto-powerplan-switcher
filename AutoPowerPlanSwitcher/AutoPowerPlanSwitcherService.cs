﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Diagnostics;
using System.Configuration;
using System.Media;

namespace AutoPowerPlanSwitcher
{
    public partial class AutoPowerPlanSwitcherService : ServiceBase
    {
        protected Timer timer;
        protected Dictionary<string,PerformanceCounter> performanceCounters;
        protected PerformanceCounterCategory performanceCounterCategory;
        protected string[] instanceNames;

        protected string[] currentPowerPlan;
        protected bool isHighPerformance;
        protected bool lastTargetHighPerformance;
        protected DateTime lastPlanStartTime;
        protected DateTime? transitionStartTime;
        protected SoundPlayer enableSound;
        protected SoundPlayer disableSound;
        protected AppConfiguration configuration;

        public AutoPowerPlanSwitcherService()
        {
            InitializeComponent();

            // initialise fields
            configuration = new AppConfiguration();
            currentPowerPlan = GetCurrentPowerPlan();
            isHighPerformance = string.Equals(currentPowerPlan[0], configuration.HighPerformanceGuid);
            lastPlanStartTime = DateTime.MinValue;
            transitionStartTime = null;

            // load sounds
            if (configuration.PlaySound)
            {
                var applicationPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                enableSound = new SoundPlayer(System.IO.Path.Combine(applicationPath, @"engage.wav"));
                disableSound = new SoundPlayer(System.IO.Path.Combine(applicationPath, @"disengage.wav"));
            }

            // configure timer
            timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Interval = (isHighPerformance ? configuration.PollingRateHighPerformance * 1000 : configuration.PollingRatePowerSaver * 1000);

            // load cpu performance counters
            performanceCounterCategory = new PerformanceCounterCategory("Processor Information");
            instanceNames = performanceCounterCategory.GetInstanceNames();
            performanceCounters = new Dictionary<string, PerformanceCounter>();
            foreach(var n in instanceNames)
                performanceCounters[n] = new PerformanceCounter("Processor Information", "% Processor Time", n);
        }

        protected override void OnStart(string[] args)
        {
            // query usage beforehand to workaround erroneous 100% reports
            GetCurrentUsage();
            GetCurrentUsage();

            // start background processing via timer
            timer.Start();
        }

        protected override void OnStop()
        {
            timer.Stop();
        }

        protected void OnTimer(object sender, ElapsedEventArgs e)
        {
            // get cpu usage counters
            var usage = GetCurrentUsage();
            var coreMax = usage.Where(x => !x.Key.Contains("_Total")).Select(x => x.Value).Max();
            var totalMax = usage.Where(x => x.Key.Contains("_Total")).Select(x => x.Value).Max();

            // select target usage levels
            var targetTotalUsage = isHighPerformance ? configuration.PowerSaverUsageThresholdBelow : configuration.HighPerformanceUsageThresholdAbove;
            var targetSingleUsage = isHighPerformance ? configuration.PowerSaverSingleCoreUsageThresholdBelow : configuration.HighPerformanceSingleCoreUsageThresholdAbove;
            Debug.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.ffff} I:{coreMax:0}%/{targetSingleUsage:0} T:{totalMax:0}%/{targetTotalUsage:0} {(transitionStartTime.HasValue ? "*" : "")}");
            
            // determine if targeting high or low performance based on current usage
            bool? targetHighPerformance = null;
            float targetInterval;
            if (isHighPerformance)
            {
                if (totalMax < targetTotalUsage || coreMax < targetSingleUsage)
                {
                    targetHighPerformance = false;
                    // start transition if required
                    if (transitionStartTime == null) transitionStartTime = DateTime.Now;
                    targetInterval = configuration.PollingRateTransitionSec * 1000;
                }
                else
                {
                    targetInterval = configuration.PollingRateHighPerformance * 1000;
                    transitionStartTime = null;
                }
            }
            else
            {
                if (totalMax > targetTotalUsage || coreMax > targetSingleUsage)
                {
                    targetHighPerformance = true;
                    // start transition if required
                    if (transitionStartTime == null) transitionStartTime = DateTime.Now;
                    targetInterval = configuration.PollingRateTransitionSec * 1000;
                }
                else
                {
                    targetInterval = configuration.PollingRatePowerSaver * 1000;
                    transitionStartTime = null;
                }
            }

            // change timer interval if desired
            if(targetInterval != timer.Interval)
            {
                timer.Stop();
                timer.Interval = targetInterval;
                timer.Start();
            }

            if (!isHighPerformance 
                && targetHighPerformance == true 
                && transitionStartTime.HasValue
                && DateTime.Now - transitionStartTime.Value > TimeSpan.FromSeconds(configuration.HighPerformanceTransitionRequiredDuration))
            {
                SwitchPlans(highPerformance: true);
                Debug.WriteLine($"Engaged high performance");
            }

            if (isHighPerformance 
                && targetHighPerformance == false
                && DateTime.Now - lastPlanStartTime > TimeSpan.FromSeconds(configuration.HighPerformanceMinimumDuration)
                && transitionStartTime.HasValue
                && DateTime.Now - transitionStartTime.Value > TimeSpan.FromSeconds(configuration.PowerSaverTransitionRequiredDuration))
            {
                SwitchPlans(highPerformance: false);
                Debug.WriteLine($"Engaged power saver");
            }
        }

        protected Dictionary<string,float> GetCurrentUsage()
        {
            var values = new Dictionary<string, float>();
            foreach (var pc in performanceCounters)
            {
                values[pc.Key] = pc.Value.NextValue();
            }

            return values;
        }

        protected string[] GetCurrentPowerPlan()
        {
            var command = RunProcess("powercfg", "/GETACTIVESCHEME");
            command = command.Replace("Power Scheme GUID: ", "");
            var elements = command.Split(new string[] { " " }, 2, StringSplitOptions.RemoveEmptyEntries);
            elements[1] = elements[1].Trim('(', ')');
            return elements;
        }

        protected void SwitchPlans(bool highPerformance)
        {
            var guid = highPerformance ? configuration.HighPerformanceGuid : configuration.PowerSaverGuid;
            SetPowerPlan(guid);
            isHighPerformance = highPerformance;
            lastPlanStartTime = DateTime.Now;
            transitionStartTime = null;

            if (configuration.PlaySound && highPerformance) enableSound.Play();
            else if (configuration.PlaySound && !highPerformance) disableSound.Play();
        }

        protected void SetPowerPlan(string guid)
        {
            var command = RunProcess("powercfg", $"/S {guid}");
            return;
        }

        protected string RunProcess(string command, string arguments = "")
        {
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = command;
            p.StartInfo.Arguments = arguments;
            p.Start();
            
            string result = p.StandardOutput.ReadToEnd();
            string error = p.StandardError.ReadToEnd();
            p.WaitForExit();

            if (p.ExitCode == 0) return result;

            var ex = new Exception($"{command} {arguments} finished with exit code {p.ExitCode}:\r\n" + result + "\r\n" + error);
            throw ex;
        }
    }
}
