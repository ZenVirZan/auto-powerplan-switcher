@echo off
goto check_permissions

:check_permissions   
    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Administrative permissions confirmed.
    ) else (
        echo Current permissions inadequate. Please run as Administrator.
		pause >nul
		goto exit
    )

"%WINDIR%\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe" /u "%~dp0\AutoPowerPlanSwitcher.exe"
pause

:exit

