# Auto PowerPlan Switcher

## Overview
Auto PowerPlan Switcher is a Windows service that automatically changes the system power plan based on current CPU usage.

This project was initiated as a result of my R9 5950X's power consumption in the default (and recommended) Windows "Balanced" power plan; I found it consumed 60-90W at idle which is insane for no load, quickly becoming a nuisance in terms of bedroom ambient temperature.

Dropping down to the "Power saver" plan resulted in 40-45W draw at idle, which is much more manageable over long periods of time, but I'd frequently forget to drop it back to Power saver and only realise after the room has warmed up.

This application resolves this dilemma.

## Dependencies
Requires .NET Framework 4.7.2

## Getting Started

1. Download the latest version from the Releases tab
1. Extract it somewhere it can stay (not a temporary location)
1. Configure the `AutoPowerPlanSwitcher.exe.config` file. To get your power plan GUIDs, run `powercfg /L` in Command Prompt
1. Run `install.bat` to install the service (as administrator)

To remove the service, run `uninstall.bat` (as administrator)

## Configuration

| Key name | Units | Description |
|----------|-------|-------------|
|HighPerformancePlanGuid|GUID|The GUID of the desired "high performance mode" power plan|
|PowerSaverPlanGuid|GUID|The GUID of the desired "power saving mode" power plan|
|HighPerformanceUsageThresholdAbove|%|The maximum allowed total CPU utilisation before triggering High Performance mode|
|HighPerformanceSingleCoreUsageThresholdAbove|%|The maximum allowed single-core usage before triggering High Performance mode|
|HighPerformanceMinimumDuration|sec|The minimum duration to stay in High Performance mode (to prevent rapid switching), although this is better controlled by the Power saver transition configuration|
|HighPerformanceTransitionRequiredDuration|sec|The usage must exceed the configured High performance mode thresholds for this period of time to trigger a plan change (to prevent spikes causing plan swaps)
|PowerSaverUsageThresholdBelow|%|The minimum allowed total CPU utilisation before transitioning back to Power saver mode
|PowerSaverSingleCoreUsageThresholdBelow|%|The minimum allowed single-core usage before transitioning back to Power saver mode 
|PowerSaverTransitionRequiredDuration|sec|The usage must be below the configured Power saver mode thresholds for this period of time to trigger a plan change (to prevent spikes causing plan swaps)
|PollingRateTransitionSec|sec|Polling frequency while in a transition period, checking to ensure usage stays above/below the required thresholds
|PollingRatePowerSaverSec|sec|Polling frequency while in Power saver mode
|PollingRateHighPerformanceSec|sec|Polling frequency while in High performance mode
|PlaySound|bool|Play a sound when switching power plans